# DA — Технології обробки великих даних

### Програмне забезпечення
* ClickHouse [[site]](https://clickhouse.com/) [[documentation]](https://clickhouse.com/docs)
* ClickHouse driver for Metabase [[site]](https://github.com/ClickHouse/metabase-clickhouse-driver)
* Grafana [[site]](https://grafana.com/grafana/) [[documentation]](https://grafana.com/docs/grafana/latest/)
* Metabase [[site]](https://www.metabase.com/) [[documentation]](https://www.metabase.com/docs/latest/)
